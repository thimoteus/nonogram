module Test.Main where

import Prelude

import Control.Monad.Error.Class (throwError)
import Data.Maybe (Maybe(..))
import Effect (Effect, forE)
import Effect.Class.Console as Console
import Effect.Exception (error)
import Effect.Random (random)
import Grid as Grid

expect :: forall a. Eq a => Show a => String -> a -> a -> Effect Unit
expect msg x y = do
  if x == y then
    Console.log (msg <> " ✅")
  else
    throwError (error ("Expected " <> show x <> " == " <> show y))

diagTest :: Effect Unit
diagTest = do
  let diag = Grid.fromFn { height: 5, width: 5 } \(Grid.Coord y x) -> y == x
  forE 0 4 \i -> do
    expect "Diagonal grid should show ones" (Just true) (Grid.index diag (Grid.Coord i i))

transposeTest :: Effect Unit
transposeTest = do
  grid <- Grid.replicateM { width: 5, height: 5 } ((_ < 0.5) <$> random)
  let gridT = Grid.transpose grid
  forE 0 5 \y -> forE 0 5 \x -> do
    let
      aij = Grid.index grid (Grid.Coord y x)
      bji = Grid.index gridT (Grid.Coord x y)
    expect
      "col should be the transpose"
      aij
      bji

showTest :: Effect Unit
showTest = do
  grid <- Grid.replicateM { width: 5, height: 5 } ((_ < 0.5) <$> random)
  Console.log (Grid.print grid)
  Console.log (Grid.print (Grid.transpose grid))

dimTest :: Effect Unit
dimTest = do
  grid <- Grid.replicateM { width: 5, height: 5 } ((_ < 0.5) <$> random)
  Console.logShow (Grid.dimensions grid)
  let transpose = Grid.transpose grid
  Console.logShow (Grid.dimensions transpose)

main :: Effect Unit
main = do
  diagTest
  transposeTest
  showTest
  dimTest
