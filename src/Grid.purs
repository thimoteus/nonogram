module Grid where

import Prelude

import Control.MonadZero (guard)
import Data.Array as Array
import Data.FoldableWithIndex (class FoldableWithIndex, foldMapWithIndex, foldlWithIndexDefault, foldrWithIndex, foldrWithIndexDefault)
import Data.FunctorWithIndex (class FunctorWithIndex, mapWithIndex)
import Data.Lens (Traversal', wander, (%~))
import Data.Lens.Index (class Index, ix)
import Data.Maybe (Maybe, fromMaybe, maybe)
import Data.Traversable (class Foldable, class Traversable, foldlDefault, foldrDefault, sequenceDefault, traverse)
import Data.TraversableWithIndex (class TraversableWithIndex, forWithIndex, traverseWithIndex)

data Coord = Coord Int Int

newtype Grid a = Grid (Array (Array a))

type Dimensions = { height :: Int, width :: Int }

derive instance functor :: Functor Grid
derive instance eq :: Eq a => Eq (Grid a)

instance semigroup :: Semigroup (Grid a) where
  append (Grid g) (Grid g') = Grid (g <> g')

instance monoid :: Monoid (Grid a) where
  mempty = Grid []

instance foldable :: Foldable Grid where
  foldMap f (Grid g) = Array.foldMap (Array.foldMap f) g
  foldr a b c = foldrDefault a b c
  foldl a b c = foldlDefault a b c

instance traversable :: Traversable Grid where
  traverse f (Grid g) = Grid <$> traverse (traverse f) g
  sequence g = sequenceDefault g

instance indexGrid :: Index (Grid a) Coord a where
  ix :: Coord -> Traversal' (Grid a) a
  ix coord = wander \coalg grid ->
    index grid coord #
      maybe
        (pure grid)
        (coalg >>> map \x -> fromMaybe grid (updateAt coord x grid))

instance foldableWithIndex :: FoldableWithIndex Coord Grid where
  foldMapWithIndex f (Grid g) = foldMapWithIndex (\y row -> foldMapWithIndex (\x col -> f (Coord y x) col) row) g
  foldrWithIndex a b c = foldrWithIndexDefault a b c
  foldlWithIndex a b c = foldlWithIndexDefault a b c

instance functorWithIndex :: FunctorWithIndex Coord Grid where
  mapWithIndex f (Grid g) = Grid (Array.mapWithIndex (\y row -> Array.mapWithIndex (\x col -> f (Coord y x) col) row) g)

instance traversableWithIndex :: TraversableWithIndex Coord Grid where
  traverseWithIndex k (Grid g) = Grid <$> forWithIndex g \y -> traverseWithIndex \x -> k (Coord y x)

rows :: forall a. Grid a -> Array (Array a)
rows (Grid g) = g

cols :: forall a. Grid a -> Array (Array a)
cols grid = do
  let { width: height } = dimensions grid
  foldrWithIndex go (Array.replicate height []) grid
  where
  go (Coord _ x) a acc = ix x %~ Array.cons a $ acc

transpose :: Grid ~> Grid
transpose = Grid <<< cols

replicate :: forall a. Dimensions -> a -> Grid a
replicate { height, width } s = Grid (Array.replicate height (Array.replicate width s))

replicateM :: forall m a. Applicative m => Dimensions -> m a -> m (Grid a)
replicateM dim s = fromFnM dim (\_ -> s)

fromFn :: forall a. Dimensions -> (Coord -> a) -> Grid a
fromFn dim = mapWithIndex (#) <<< replicate dim

fromFnM :: forall m a. Applicative m => Dimensions -> (Coord -> m a) -> m (Grid a)
fromFnM dim = traverseWithIndex (#) <<< replicate dim

index :: forall a. Grid a -> Coord -> Maybe a
index (Grid g) (Coord y x) = do
  row <- g Array.!! y
  row Array.!! x

updateAt :: forall a. Coord -> a -> Grid a -> Maybe (Grid a)
updateAt (Coord y x) v (Grid g) = Grid <$> do
  row <- g Array.!! y
  newRow <- Array.updateAt x v row
  Array.updateAt y newRow g

dimensions :: forall a. Grid a -> Dimensions
dimensions (Grid g) = { height, width }
  where
  height = Array.length g
  width = fromMaybe 0 (Array.length <$> Array.head g)

-- | Adds a row at the top of the grid if the length of the input array matches the width of the grid.
consRow :: forall a. Array a -> Grid a -> Maybe (Grid a)
consRow row g@(Grid grid) = ado
  guard (Array.length row == (dimensions g).width)
  in Grid (Array.cons row grid)

-- | Adds a row at the bottom of the grid if the length of the input array matches the width of the grid.
snocRow :: forall a. Grid a -> Array a -> Maybe (Grid a)
snocRow g@(Grid grid) row = ado
  guard (Array.length row == (dimensions g).width)
  in Grid (Array.snoc grid row)

-- | Adds a column at the left of the grid if the length of the input array matches the height of the grid.
consCol :: forall a. Array a -> Grid a -> Maybe (Grid a)
consCol col g@(Grid grid) = Grid <$> ado
  guard (Array.length col == (dimensions g).height)
  in Array.zipWith Array.cons col grid

-- | Adds a column at the right of the grid if the length of the input array matches the height of the grid.
snocCol :: forall a. Grid a -> Array a -> Maybe (Grid a)
snocCol g@(Grid grid) col = Grid <$> ado
  guard (Array.length col == (dimensions g).height)
  in Array.zipWith Array.snoc grid col

print :: forall a. Show a => Grid a -> String
print (Grid rs) = Array.intercalate "\n" (map showRow rs)
  where
  showRow = Array.intercalate ", " <<< map show
