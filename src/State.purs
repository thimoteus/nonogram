module State where

import Prelude

import Data.Array as Array
import Data.Array.NonEmpty as NEA
import Data.Function (on)
import Data.Lens ((%~))
import Data.Lens.Index (ix)
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Random (random)
import Grid (Grid)
import Grid as Grid

data Fullness = Empty | Full
derive instance eqFullness :: Eq Fullness

data Line = Row | Col
derive instance eqLine :: Eq Line

type Square =
  { guess :: Maybe Fullness
  , actual :: Fullness
  }

data Difficulty = Easy | Normal | Hard

type State =
  { grid :: Grid Square
  , marker :: Fullness
  , rowMetadata :: Array (Array Int)
  , colMetadata :: Array (Array Int)
  , difficulty :: Difficulty
  , dimensions :: Grid.Dimensions
  , error :: Maybe Grid.Coord
  , correct :: Int
  }

initialState :: State
initialState =
  { grid: mempty
  , marker: Full
  , rowMetadata: []
  , colMetadata: []
  , difficulty: Normal
  , dimensions: { height: 15, width: 15 }
  , error: Nothing
  , correct: 0
  }

flipFullness :: Fullness -> Fullness
flipFullness = case _ of
  Full -> Empty
  Empty -> Full

fullnessClass :: Maybe Fullness -> String
fullnessClass = case _ of
  Just Full -> "full"
  Just Empty -> "empty"
  Nothing -> "unknown"

coordClass :: Grid.Coord -> String
coordClass (Grid.Coord y x) = lineClass Row y <> "-" <> lineClass Col x

lineClass :: Line -> Int -> String
lineClass ln n = (_ <> show n) case ln of
  Row -> "row"
  Col -> "col"

getMetadata :: Grid Square -> { row :: Array (Array Int), col :: Array (Array Int) }
getMetadata g = { row: getInts <$> Grid.rows g, col: getInts <$> Grid.cols g }
  where
  getInts = map NEA.length
    <<< Array.filter (eq Full <<< _.actual <<< NEA.head)
    <<< Array.groupBy (eq `on` _.actual)

markEmpty :: Grid.Coord -> Grid Square -> Grid Square
markEmpty coord = ix coord %~ _ { guess = Just Empty }

markFull :: Grid.Coord -> Grid Square -> Grid Square
markFull coord = ix coord %~ _ { guess = Just Full }

mkGrid :: Grid.Dimensions -> Number -> Effect (Grid Square)
mkGrid dim p = Grid.replicateM dim ado
  num <- random
  let
    guess = Nothing
    actual = if num < p then Full else Empty
  in { guess, actual }
