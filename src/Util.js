exports.timedFFI = function(label) {
  return function(k) {
    console.time(label);
    const result = k();
    console.timeEnd(label);
    return result;
  };
};
