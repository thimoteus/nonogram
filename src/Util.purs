module Util
  ( cl
  , timed
  ) where

import Prelude

import Debug.Trace (class DebugWarning)
import Halogen as H
import Halogen.HTML.Properties as HP

cl :: forall r i. String -> HP.IProp ( class :: String | r ) i
cl = HP.class_ <<< H.ClassName

foreign import timedFFI :: forall a. String -> (Unit -> a) -> a

timed :: forall a. DebugWarning => String -> (Unit -> a) -> a
timed = timedFFI
