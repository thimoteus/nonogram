module Main where

import Prelude

import Action (Action(..))
import Data.Array as Array
import Data.Foldable (for_)
import Data.FoldableWithIndex (foldMapWithIndex)
import Data.Lens ((^?))
import Data.Lens.Index (ix)
import Data.Maybe (Maybe(..), fromMaybe)
import Effect (Effect)
import Effect.Class (class MonadEffect)
import Effect.Class.Console as Console
import Grid as Grid
import Halogen as H
import Halogen.Aff as HA
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.VDom.Driver (runUI)
import State (Difficulty(..), Fullness(..), Line(..), Square, State)
import State as State
import Util as U
import Web.Event.Event (preventDefault)
import Web.UIEvent.MouseEvent as ME

type HTML m = H.ComponentHTML Action () m

handleAction :: forall o m. MonadEffect m => Action -> H.HalogenM State Action () o m Unit
handleAction = case _ of
  MkGrid -> do
    { difficulty, dimensions } <- H.get
    let
      p = case difficulty of
        Easy -> 0.75
        Normal -> 0.7
        Hard -> 0.65
    grid <- H.liftEffect (State.mkGrid dimensions p)
    let { row, col } = State.getMetadata grid
    H.put State.initialState { grid = grid, rowMetadata = row, colMetadata = col }

  MarkEmpty coord -> U.timed "MarkEmpty" \_ -> do
    handleAction (CheckValid coord Empty)
    H.modify_ \st -> st { grid = State.markEmpty coord st.grid, correct = st.correct + 1 }

  MarkFull coord -> U.timed "MarkFull" \_ -> do
    handleAction (CheckValid coord Full)
    H.modify_ \st -> st { grid = State.markFull coord st.grid, correct = st.correct + 1 }

  CheckValid coord fullness -> do
    grid <- H.gets _.grid
    let
      hasError = fromMaybe false do
        sq <- grid ^? ix coord
        pure (sq.actual /= fullness)
    if hasError then
      H.modify_ _ { error = Just coord }
    else
      pure unit

  ChangeFullness -> do
    fullness <- H.gets _.marker
    H.modify_ _ { marker = State.flipFullness fullness }

  PreventDefault ev act ->
    H.liftEffect (preventDefault ev) *> for_ act handleAction

  Debug str act -> Console.logShow str *> for_ act handleAction

renderApp :: forall m. State -> HTML m
renderApp st = HH.div
  [ HP.id_ "app" ]
  [ renderByState ]
  where
  renderByState :: HTML m
  renderByState
    | Just err <- st.error = renderError err
    | { height, width } <- st.dimensions
    , st.correct == height*width = renderSolved
    | otherwise = renderBoard

  renderBoard :: HTML m
  renderBoard = do
    let { grid, rowMetadata, colMetadata } = st
    HH.div
      [ U.cl "board" ]
      [ HH.div
          [ U.cl "toggle" ]
          [ HH.a
              [ U.cl "fullnessToggle"
              , HE.onClick \_ -> Just ChangeFullness
              , HP.href "#"
              ]
          [ HH.text $ "Marking as " <> case st.marker of
            Full -> "full"
            Empty -> "empty"
          ]
      ]
      , HH.div
          [ U.cl "colMetadata" ]
          (Array.mapWithIndex (render Col) colMetadata)
      , HH.div
          [ U.cl "rowMetadata" ]
          (Array.mapWithIndex (render Row) rowMetadata)
      , HH.div
          [ U.cl "grid" ]
          (foldMapWithIndex renderCell grid)
      ]
    where
    render :: Line -> Int -> Array Int -> HTML m
    render ln rowNum ints = HH.div
      [ U.cl (State.lineClass ln rowNum) ]
      (renderMetaInfo ln <$> ints)

    renderMetaInfo :: Line -> Int -> HTML m
    renderMetaInfo ln n = case ln of
      Row -> HH.span
        [ U.cl "rowMetadatum" ]
        [ HH.text (show n) ]
      Col -> HH.div
        [ U.cl "colMetadatum" ]
        [ HH.text (show n) ]

  renderCell :: Grid.Coord -> Square -> Array (HTML m)
  renderCell coord { guess, actual } = pure $ HH.div
    [ HP.classes
        [ H.ClassName "square"
        , fullnessClass guess
        , coordClass coord
        ]
    , HE.onMouseUp (handleClick coord guess)
    ]
    [ renderGuess ]
    where
    renderGuess :: HTML m
    renderGuess = case guess of
      Just Empty -> HH.text "x"
      Just Full -> HH.text ""
      Nothing -> HH.text ""

  handleClick :: Grid.Coord -> Maybe Fullness -> ME.MouseEvent -> Maybe Action
  handleClick coord fullness ev = case fullness of
    Just _ -> Nothing
    _ -> Just case st.marker, ME.button ev of
      Full, 2 -> PreventDefault (ME.toEvent ev) (Just (MarkEmpty coord))
      Empty, 2 -> PreventDefault (ME.toEvent ev) (Just (MarkFull coord))
      Full, _ -> MarkFull coord
      Empty, _ -> MarkEmpty coord

  fullnessClass :: Maybe Fullness -> H.ClassName
  fullnessClass = H.ClassName <<< State.fullnessClass

  coordClass :: Grid.Coord -> H.ClassName
  coordClass = H.ClassName <<< State.coordClass

  renderError :: Grid.Coord -> HTML m
  renderError coord = HH.div
    [ U.cl "error" ]
    [ HH.a
        [ U.cl "reset"
        , HE.onClick \_ -> Just MkGrid
        , HP.href "#"
        ]
        [ HH.text "Incorrect guess ☹ Reset?" ]
    ]

  renderSolved :: HTML m
  renderSolved = HH.div
    [ U.cl "solved" ]
    [ HH.a
        [ U.cl "reset"
        , HE.onClick \_ -> Just MkGrid
        , HP.href "#"
        ]
        [ HH.text "You won! Reset?" ]
    ]

component :: forall q i o m. MonadEffect m => H.Component HH.HTML q i o m
component = H.mkComponent
  { initialState: \_ -> State.initialState
  , render: renderApp
  , eval: H.mkEval H.defaultEval
      { handleAction = handleAction
      , initialize = Just MkGrid
      }
  }

main :: Effect Unit
main = HA.runHalogenAff do
  body <- HA.awaitBody
  runUI component unit body
