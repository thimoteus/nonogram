module Action where

import Data.Maybe (Maybe)
import Grid as Grid
import State (Fullness)
import Web.Event.Internal.Types (Event)

data Action
  = MkGrid
  | MarkEmpty Grid.Coord
  | MarkFull Grid.Coord
  | CheckValid Grid.Coord Fullness
  | ChangeFullness
  | PreventDefault Event (Maybe Action)
  | Debug String (Maybe Action)
